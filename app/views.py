from django.shortcuts import render, get_object_or_404
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.generics import ListCreateAPIView \
    , RetrieveUpdateDestroyAPIView, CreateAPIView, ListAPIView
from rest_framework.utils import json
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.authtoken.views import ObtainAuthToken

from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions

from django.utils.timezone import now

from app.models import Exam, Question, Answer, Test, List
from app.serializers import ExamSerializer, QuestionSerializer, \
    AnswerSerializer, UserSerializer, TestSeri, ListSerializer


class ExamsViewSet(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ExamSerializer

    def get_queryset(self):
        queryset = Exam.objects.all()
        id = self.request.query_params.get('id', None)
        username = self.request.query_params.get('username', None)
        if id is not None:
            queryset = queryset.filter(id=id)
        if username is not None:
            queryset = queryset.filter(author=username)
        return queryset

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]
            # check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super(ExamsViewSet, self).get_serializer(*args, **kwargs)

    def post(self, request, format='json'):
        data = request.data
        str = request.META.get('HTTP_AUTHORIZATION')
        str = str.split()
        token = str[1]

        serializer = ExamSerializer(data=data, context={"token": token})
        if serializer.is_valid():
            exam = serializer.save()
            if exam:
                return Response({"status": "good", "id": exam.id})
            else:
                return Response("bad")


class ListViewSet(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = List.objects.all()
    serializer_class = ListSerializer

    def get_queryset(self):
        queryset = List.objects.all()
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(author=username)
        return queryset

    def post(self, request, format='json'):
        data = request.data
        str = request.META.get('HTTP_AUTHORIZATION')
        str = str.split()
        token = str[1]
        print(token)

        serializer = ListSerializer(data=data, context={"token": token})

        if serializer.is_valid():
            list1 = serializer.save()
            if list1:
                return Response("good")
            else:
                return Response(serializer.errors)
        else:
            return Response(serializer.errors)


class QuesViewSet(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = QuestionSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Question.objects.all()
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(exam=id)
        return queryset

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]
            # check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super(QuesViewSet, self).get_serializer(*args, **kwargs)

    def post(self, request, format='json'):
        data = request.data
        # if request with one question
        if type(data) is dict:
            serializer = QuestionSerializer(data=data)
            if serializer.is_valid():
                que = serializer.save()
                if que:
                    return Response("good")
                else:
                    return Response("bad")

        count = 0
        # if request with more than one question
        for data1 in data:
            serializer = QuestionSerializer(data=data1)
            if serializer.is_valid():
                que = serializer.save()
                if que:
                    count += 1
        if count == data.__len__():
            return Response("good", status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors)


class UserV(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.get(user=user)
                if user.is_staff:
                    is_staff = True
                else:
                    is_staff = False
                context = {
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'token': token.key,
                    'user_id': user.pk,
                    'email': user.email,
                    'username': user.username,
                    'is_staff': is_staff
                }
                return Response(context)
        else:
            return Response(serializer.errors)


class ChangePassword(CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User, username=request.user)
        user.get_password(request.POST.get("new_password"))

        user.save()

        return Response({'detail': 'Password has been saved.'})


class AnswerViewSet(ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def list(self, request):
        queryset = Answer.objects.all()
        serializer = AnswerSerializer(queryset, many=True)
        if request.method == 'POST':
            print(9999999999998)
            for q in Question.objects.all():
                print(585858)
                if (q.type == "mcq"):
                    for a in Answer.objects.all():
                        if (str(a.question) == str(q.content)):
                            if (a.answer == q.correct_answer):
                                a.last_degree = q.degree
                            else:
                                a.last_degree = 0
                            a.save()

                q.save()
            return Response("jjjj")
        else:
            return Response(serializer.data)


class AnswerApi(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def get_queryset(self):
        queryset = Answer.objects.all()
        id = self.request.query_params.get('id', None)
        username = self.request.query_params.get('username', None)
        if id is not None:
            queryset = queryset.filter(exam=id)
        if username is not None:
            queryset = queryset.filter(author=username)
        return queryset

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]
            # check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super(AnswerApi, self).get_serializer(*args, **kwargs)

    def post(self, request, format='json'):
        data = request.data
        str = request.META.get('HTTP_AUTHORIZATION')
        str = str.split()
        token = str[1]
        author = User.objects.get(auth_token=token)

        # if request with one Answer
        if type(data) is dict:
            serializer = AnswerSerializer(data=data, context={"token": token})
            if serializer.is_valid():
                ans = serializer.save()
                if ans:
                    return Response("good", status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors)

        count = 0
        # if request with more than one Answer
        for item in data:
            serializer = AnswerSerializer(data=item, context={"token": token})
            if serializer.is_valid():
                ans = serializer.save()
                if ans:
                    count += 1
        if count == data.__len__():
            return Response("good", status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors)


class DegreeApi(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def post(self, request, format='json'):
        str = request.META.get('HTTP_AUTHORIZATION')
        str = str.split()
        token = str[1]
        author = User.objects.get(auth_token=token)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        exam = body['exam']
        print(author)

        for e in Exam.objects.all():
            if e.id == exam:
                exam = e
                break

        degree = 0
        total = 0

        a = Answer.objects.filter(author=author)
        que = Question.objects.filter(exam=exam)

        for q in que:
            total += q.score

        ############################3
        s = {}
        obj = {}
        count = 1

        for q in que:
            count = count.__str__()
            qsco = "s" + count
            s["score"] = q.score
            for ans in a:
                if ans.question == q:
                    s["degree"] = ans.last_degree
                    break
            obj[qsco] = s
            count = int(count)
            count += 1
            s = {}

        ###########################

        for ans in a:
            if ans.exam == exam:
                degree += ans.last_degree

        obj["total"] = total
        obj["degree"] = degree

        return Response(obj, status=status.HTTP_201_CREATED)


class StudentsList(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def post(self, request, format='json'):
        str = request.META.get('HTTP_AUTHORIZATION')
        str = str.split()
        token = str[1]
        author = User.objects.get(auth_token=token)
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        exam = body['exam']
        print(author)

        degree = 0
        total = 0
        obj = {}

        que = Question.objects.filter(exam=exam)
        for q in que:
            total += q.score

        ans = Answer.objects.filter(exam=exam)
        for a in ans:
            auth = a.author
            username = auth.username
            obj[username] = 0
        for a in ans:
            auth = a.author
            username = auth.username
            obj[username] += a.last_degree

        return Response(obj, status=status.HTTP_201_CREATED)


class Log(ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = User.objects.all()

    def post(self, request, format='json'):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        username = body['username']
        password = body['password']

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response("User Does Not Exist")

        if user.password == password:
            token, created = Token.objects.get_or_create(user=user)
            context = {
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    "token": token.key,
                    "user_id": user.pk,
                    'email': user.email,
                    'username': user.username,
                    "is_staff": user.is_staff}
            return Response(context)

        else:
            return Response("Username or Password is not correct")


class TestViewSet(ModelViewSet):
    permission_classes = (permissions.AllowAny,)
    queryset = Test.objects.all()
    serializer_class = TestSeri

    def get_serializer(self, *args, **kwargs):
        if "data" in kwargs:
            data = kwargs["data"]

            # check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super(TestViewSet, self).get_serializer(*args, **kwargs)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email,
            'is_staff': user.is_staff
        })

