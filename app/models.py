from django.db import models
from django.utils import timezone
from django.utils.timezone import now
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.db.models.signals import post_save
from django.contrib.auth.models import User


class Exam (models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    # author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    id_front = models.IntegerField(default=0)
    created_data = models.DateTimeField(default=now())
    published_date = models.DateTimeField(blank=True, null=True)
    total_degree = models.IntegerField(null=True)
    duration = models.CharField(null=True, max_length=100)
    # user = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class List (models.Model):  # list of exams for users
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    author = models.CharField(max_length=200, default="sd")
    name = models.CharField(max_length=200, null=True, default="")
    published_date = models.DateTimeField(blank=True, null=True)
    total_degree = models.IntegerField(null=True, default=100)
    duration = models.CharField(null=True, max_length=100, default="2h")
    score = models.IntegerField(null=True, default=100)


class Question(models.Model):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    type = models.CharField(max_length=100)
    id_que = models.IntegerField(default=0)
    content = models.TextField()
    correct_answer = models.TextField()
    score = models.FloatField()
    qNumber = models.IntegerField(default=0)
    qSubNumber = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=100, null=True)
    choises = models.CharField(null=True, max_length=100)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE,null=True)
    answer = models.TextField()
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    last_degree = models.FloatField(default=-1, null=True)


class Test(models.Model):
    name = models.CharField(max_length=50)
    i = models.IntegerField(default=0)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


