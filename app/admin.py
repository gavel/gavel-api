from django.contrib import admin
from . models import Exam, Question, Answer, Test, List
from rest_framework.authtoken.admin import TokenAdmin


class ExamAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name', ]


TokenAdmin.raw_id_fields = ('user',)

admin.site.register(Exam)
admin.site.register(List)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Test)

