from rest_framework import serializers
from rest_framework.serializers import (
    ValidationError,
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField
)
from app.models import Exam, Question, Answer, Test, List
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import ast
#from .textsim.load_utils import *


class ExamSerializer(serializers.ModelSerializer):
    def create(self, validate_data):
        name = validate_data["name"]
        author = validate_data["author"]
        total_degree = validate_data["total_degree"]
        published = validate_data["published_date"]
        duration = validate_data["duration"]

        token = self.context["token"]

        author = User.objects.get(auth_token=token)
        username = author.username

        exam = Exam(name=name, author=username, total_degree=total_degree
                    , published_date=published, duration=duration)
        exam.save()

        return exam

    class Meta:
        model = Exam
        fields = "__all__"


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = List
        fields = "__all__"

    def create(self, validate_data):
        exam = validate_data["exam"]
        author = validate_data["author"]
        token = self.context["token"]

        author = User.objects.get(auth_token=token)

        for e in Exam.objects.all():
            if e.id == exam:
                exam = e
                break

        username = author.username
        name = exam.name
        pu = exam.published_date
        duration = exam.duration
        total = exam.total_degree

        li = List(exam=exam, author=username, name=name, published_date=pu, duration=duration, total_degree=total)
        li.score = -1

        li.save()

        return li


class QuestionSerializer(serializers.ModelSerializer):

    def create(self, validate_data):
        typ = validate_data["type"]
        content = validate_data["content"]
        correct_answer = validate_data["correct_answer"]
        score = validate_data["score"]
        qNum = validate_data["qNumber"]
        qSub = validate_data["qSubNumber"]
        title = validate_data["title"]
        choises = validate_data["choises"]
        exam = validate_data["exam"]

        for e in Exam.objects.all():
            if e.id == exam:
                exam = e
                break

        # MyModel.objects.create(**validated_data)

        que = Question(type=typ, content=content, correct_answer=correct_answer
                       , score=score, qNumber=qNum, qSubNumber=qSub
                       , title=title, choises=choises, exam=exam)
        que.save()

        return que

    class Meta:
        model = Question
        fields = "__all__"


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = "__all__"

    def create(self, validate_data):
        print(type(validate_data))
        question = validate_data["question"]
        exam = validate_data["exam"]
        answer = validate_data["answer"]
        author = validate_data["author"]
        token = self.context["token"]
        author = User.objects.get(auth_token=token)

        for e in Exam.objects.all():
            if e.id == exam:
                exam = e
                break

        global db_question
        for que in Question.objects.all():
            if que == question:
                db_question = que
                break

        ans = Answer(question=question, exam=exam, answer=answer, author=author)

        if db_question.type == "mcq":
            if db_question.correct_answer == answer:
                ans.last_degree = db_question.score
            else:
                ans.last_degree = 0

        elif db_question.type == "code":
            x = (ans.answer).find("null")
            if x > 0:
                ans.last_degree = 0
                ans.save()
                return ans

            s1 = ans.answer
            s1 = s1.replace("\n", "")
            obj_ans = ast.literal_eval(s1)
            # obj_ans = obj_ans[1]
            output_ans = obj_ans["stdout"]

            time_ans = obj_ans["time"]
            memory_ans = obj_ans["memory"]

            obj_question = ast.literal_eval(db_question.choises)
            obj_que = obj_question[1]
            output_que = db_question.correct_answer
            time_que = float(obj_que["time"])
            memory_que = float(obj_que["memory"])

            a = output_que.find(output_ans)

            if a >= 0:
                ans.last_degree = db_question.score
                if time_que < time_ans:
                    ans.last_degree -= 1
                if memory_que < memory_ans:
                    ans.last_degree -= 1
            else:
                ans.last_degree = 0
        else:
            ans.last_degree = 0

        list = List.objects.filter(exam=exam)
        answer = Answer.objects.filter(exam=exam)

        last = 0
        for a in answer:
            auth = a.author
            if auth.username == author.username:
                last += a.last_degree

        for li in list:
            if li.author == author.username:
                l = li
                l.score = last
        l.save()

        ans.save()

        return ans


class UserSerializer(serializers.ModelSerializer):
    def create(self, validate_data):
        first_name = validate_data['first_name']
        last_name = validate_data['last_name']
        username = validate_data['username']
        email = validate_data['email']
        password = validate_data['password']
        is_staff = validate_data['is_staff']

        user = User(username=username, email=email, password=password)
        if is_staff:
            user.is_staff = 1
        else:
            user.is_staff = 0

        user.first_name = first_name
        user.last_name = last_name

        user.save()

        return user

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password', 'is_staff']
        extra_kwargs = {"password": {"write_only": True}}


class TestSeri(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = "__all__"

