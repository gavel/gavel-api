from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken import views as authviews

from app.views import ExamsViewSet, AnswerViewSet, TestViewSet
from . import views

router = routers.DefaultRouter()
router.register(r'answer', AnswerViewSet)
router.register(r'test', TestViewSet)

urlpatterns = [
    url(r'^reg/$', view=views.UserV.as_view()),
    url(r'^Change_password/$', view=views.ChangePassword.as_view()),
    url(r'^login/$', view=views.CustomAuthToken.as_view()),
    url(r'^log/$', view=views.Log.as_view()),
    url(r'^exams/$', view=views.ExamsViewSet.as_view()),
    url(r'^list/$', view=views.ListViewSet.as_view()),
    url(r'^studentList/$', view=views.StudentsList.as_view()),
    url(r'^ques/$', view=views.QuesViewSet.as_view()),
    url(r'^ans/$', view=views.AnswerApi.as_view()),
    url(r'^degree/$', view=views.DegreeApi.as_view()),
    url(r'^', include(router.urls)),
]

# urlpatterns = format_suffix_patterns(urlpatterns)





